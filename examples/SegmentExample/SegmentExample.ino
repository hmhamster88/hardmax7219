#include <HardMax7219.h>

#define CS_PIN 10
#define DIGIT_COUNT 4
#define BUFFER_SIZE (DIGIT_COUNT + 1)

char str[BUFFER_SIZE] = "goal";

HardMax7219 hardMax(CS_PIN, DIGIT_COUNT);

void setup() {
	Serial.begin(9600);
	hardMax.writeString(str);
}

void loop() {
}

void serialEvent() {
  	while (Serial.available()) {
	    char inChar = (char)Serial.read();
	    for (int i = 0; i < BUFFER_SIZE - 1; i++) {
	    	str[i] = str[i + 1];
	    }
	    str[BUFFER_SIZE - 2] = inChar;
	    hardMax.writeString(str);
	}
}
