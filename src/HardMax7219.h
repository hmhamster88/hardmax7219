#ifndef HARD_MAX_7219
#define HARD_MAX_7219

#include "Arduino.h"

class HardMax7219 {
	public:
		HardMax7219(int csPin, byte digitsCount);
		void setSegments(byte segments, byte place);
		void clear();
		void setDigit(byte digit, byte place);
		byte getSegmentsForChar(char ch);
		void setChar(char ch, byte place);
		void writeString(char* string);
		void setIntensity(byte value);
		void writeInt(int value, int base, int dot = -1);
	private:
		int _csPin;
		byte _digitsCount;
		void write(byte address, byte data);
};

#endif
