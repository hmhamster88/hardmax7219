#include "HardMax7219.h"
#include <SPI.h>

#define ARR_SIZE(x)  (sizeof(x) / sizeof((x)[0]))

#define DIGIT_ADDR_0 		0x01
#define DIGIT_ADDR_7 		0x08

#define DECODEMODE_ADDR 	0x09
#define INTENSITY_ADDR  	0x0A
#define SCANLIMIT_ADDR  	0x0B
#define SHUTDOWN_ADDR 		0x0C
#define TEST_MODE_ADDR		0x0F

#define DECODE_MODE_NONE 	0x00
#define SHUTDOWN_ON			0x01

#define MIN_INTENSY			0x01
#define MAX_INTENSY			0x0F

#define CLOCK_DIVIDER		2 // 16Mhz / 2 = 8Mhz
#define MAX_DIGIT_COUNT		8

				    //ABCDEFG
#define LETER_MINUS	B00000001
#define LETER_DOT	B10000000
#define LETER_SPACE	B00000000

#define CHAR_TABLE_LETTERS_INDEX 10
#define HEX_CHARS_COUNT 16

const static byte charTable [] PROGMEM  = {
	// Numbers
    B01111110,
    B00110000,
    B01101101,
    B01111001,
    B00110011,
    B01011011,
    B01011111,
    B01110000,
    B01111111,
    B01111011,
    // Letters
    B01110111,	// A
    B00011111,	// b
    B01001110,	// C
    B00111101,	// d
    B01001111,	// e
    B01000111,	// F
    B01011110,	// G
    B00110111,	// H
    B00000110,	// I
    B00111100,	// J
    B01010111,	// K
    B00001110,	// L
    B01010101,	// M
    B00010101,	// n
    B01111110,	// O
    B01100111,	// P
    B01110011,	// q
    B00000101,	// r
    B01011011,	// s
    B00001111,	// t
    B00111110,	// U
    B00100111,	// V
    B00101011,	// W
    B00010011,	// X
    B00111011,	// Y
    B01101101	// Z
};

HardMax7219::HardMax7219(int csPin, byte digitsCount) {
	_csPin = csPin;
	if (digitsCount > MAX_DIGIT_COUNT) {
  		_digitsCount = MAX_DIGIT_COUNT;
  	} else {
  		_digitsCount = digitsCount;
  	}
	pinMode(_csPin, OUTPUT);
	digitalWrite(_csPin, HIGH);
	SPI.begin();
	SPI.setClockDivider(CLOCK_DIVIDER);
	write(SCANLIMIT_ADDR, _digitsCount - 1);
	write(DECODEMODE_ADDR, DECODE_MODE_NONE);
	write(TEST_MODE_ADDR, 0);
	setIntensity(7);
	clear();
  	write(SHUTDOWN_ADDR, SHUTDOWN_ON);
}

void HardMax7219::setSegments(byte segments, byte place) {
	if (_digitsCount <= place) {
		return;
	}
	byte digitAddr = place + DIGIT_ADDR_0;
	write(digitAddr, segments);
}

void HardMax7219::clear() {
	for(int place = 0; place < _digitsCount; place++) {
		setSegments(0, place);
	}
}

void HardMax7219::setDigit(byte digit, byte place) {
	if (digit >= HEX_CHARS_COUNT) {
		return;
	}
	setSegments(pgm_read_byte(&charTable[digit]), place);
}

byte HardMax7219::getSegmentsForChar(char ch) {
	if (ch >= '0' && ch <= '9') {
		return pgm_read_byte(&charTable[ch - '0']);
	} else if (ch >= 'A' && ch <= 'Z') {
		return pgm_read_byte(&charTable[ch - 'A' + CHAR_TABLE_LETTERS_INDEX]);
	} else if (ch >= 'a' && ch <= 'z') {
		return pgm_read_byte(&charTable[ch - 'a' + CHAR_TABLE_LETTERS_INDEX]);
	} else {
		switch(ch) {
			case '-':
				return LETER_MINUS;
			case ' ':
				return LETER_SPACE;
		}
	}
	return 0;
}

void HardMax7219::setChar(char ch, byte place) {
	setSegments(getSegmentsForChar(ch), place);
}

void HardMax7219::writeString(char* string) {
	byte place = 0;
	char ch;
	while (string != 0 && place < _digitsCount) {
		ch = getSegmentsForChar((*string));
		if (*(string + 1) == '.') {
			ch |= LETER_DOT;
			string += 2;
		} else {
			string++;
		}
		setSegments(ch, place);
		place++;
	}
}

void HardMax7219::setIntensity(byte value) {
	if (value < MIN_INTENSY) {
		value = MIN_INTENSY;
	} else if(value > MAX_INTENSY) {
		value = MAX_INTENSY;
	}
	write(INTENSITY_ADDR, value);
}

void HardMax7219::write(byte address, byte data) {
	digitalWrite(_csPin, LOW);
	SPI.transfer(address);
	SPI.transfer(data);
	digitalWrite(_csPin, HIGH);
}

void HardMax7219::writeInt(int value, int base, int dot) {
	int place = _digitsCount - 1;
	bool negative = value < 0;
	if (value == 0)
	{
		setChar('0', place);
		place--;
	} else {
		while (value != 0 && place >= 0)
	    {
	        int rem = value % base;
	        char ch = (rem > 9)? (rem-10) + 'a' : rem + '0';
	        char segments = getSegmentsForChar(ch);
	        if (dot == 0) {
	        	segments |= LETER_DOT;
	        }
	        dot--;
	        setSegments(segments, place);
	        value = value/base;
	        place--;
	    }

	    if (place >= 0 && negative) {
			setChar('-', place);
	    }
	}

    while(place >= 0) {
    	setChar(' ', place);
    	place--;
    }
}
